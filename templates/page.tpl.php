<div class="container_12_d container_3_m container_9_t">

  <?php if ($page['page_top']): ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
      <?php print render($page['page_top']); ?>
    </div>
  <?php endif; ?>

  <div class="alpha omega g_12_d g_9_t g_3_m">

    <?php if ($logo): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan">

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name">
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"<?php if (isset($hide_site_slogan)) {
            print ' class="element-invisible"';
          } ?>>
          <?php print $site_slogan; ?>
          </div>
      <?php endif; ?>

      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>

<?php print render($page['header']); ?>

  </div>

    <?php if ($page['help']): ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
    <?php print render($page['help']); ?>
    </div>
  <?php endif; ?>

    <?php if ($page['highlighted']): ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
    <?php print render($page['highlighted']); ?>
    </div>
<?php endif; ?>

</div>
<div class="container_12_d container_3_m container_9_t">

    <?php if (($messages) || isset($page['featured'])) : ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
          <?php if ($messages): ?>
        <div id="messages"><div class="row clearfix">
        <?php print $messages; ?>
          </div></div> <!-- /.section, /#messages -->
      <?php endif; ?>

      <?php if (isset($page['featured'])): ?>
        <div id="featured"><div class="row clearfix">
          <?php print render($page['featured']); ?>
        </div></div> <!-- /.section, /#featured -->
      <?php endif; ?>
    </div>
    <?php endif; ?>

  <div class="alpha omega_m g_3_d g_3_t g_3_m">
      <?php if ($main_menu): ?>
      <div id="main-menu" class="navigation">
        <?php
        print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        ));
        ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>
    <?php if ($page['sidebar_first']): ?>
      <?php print render($page['sidebar_first']); ?>
    <?php endif; ?>
  </div>
  <div class="alpha_m omega_t omega_m g_6_d g_6_t g_3_m">
    <a id="main-content"></a>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <h1 class="title" id="page-title">
      <?php print $title; ?>
      </h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php print render($page['content']); ?>
  </div>
  <div class=" alpha_t alpha_m omega g_3_d g_9_t g_3_m">
    <?php print render($page['sidebar_second']); ?>
  </div>
</div>
<div class="container_12_d container_3_m container_9_t">

  <?php
  if (isset($page['footer_firstcolumn']) ||
      isset($page['footer_secondcolumn']) ||
      isset($page['footer_thirdcolumn']) ||
      isset($page['footer_fourthcolumn'])) :
    ?>
    <div class="row">
      <div class="alpha g_3_d g_3_t g_1_m">
        <?php if ($page['footer_firstcolumn']): ?>
          <?php print render($page['footer_firstcolumn']); ?>
        <?php endif; ?>
      </div>
      <div class="g_3_d g_3_t g_1_m">
        <?php if ($page['footer_secondcolumn']): ?>
          <?php print render($page['footer_secondcolumn']); ?>
        <?php endif; ?>
      </div>
      <div class="omega_t omega_m g_3_d g_3_t g_1_m">
        <?php if ($page['footer_thirdcolumn']): ?>
          <?php print render($page['footer_thirdcolumn']); ?>
        <?php endif; ?>
      </div>
      <div class="alpha_t alpha_m omega g_3_d g_9_t g_3_m">
        <?php if ($page['footer_fourthcolumn']): ?>
          <?php print render($page['footer_fourthcolumn']); ?>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($page['footer']): ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
      <?php print render($page['footer']); ?>
    </div>
  <?php endif; ?>
  <?php if ($page['page_bottom']): ?>
    <div class="alpha omega g_12_d g_9_t g_3_m">
      <?php print render($page['page_bottom']); ?>
    </div>
  <?php endif; ?>
</div>